﻿using Koolss.Core.Helper;
using Koolss.Core.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;


/**
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Koolss.Core.Io
*│　类    名： FileUtil
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：
*│　作    者：Koolss
*│　版    本：1.0.0
*│　邮    箱：koolss@koolss.com
*│　创建时间：2019/11/21 星期四 15:37:25
*│　机器名称：HL
*└──────────────────────────────────────────────────────────────┘
*/

namespace Koolss.Core.Io
{
    public class FileHelper
    {
        /// <summary>
        /// 获取文件名
        /// </summary>
        /// <param name="path">文件路径</param>
        /// <returns></returns>
        public static string GetName(string filePath)
        {
            if (StrHelper.IsBlank(filePath))
            {
                return null;
            }
            return Path.GetFileName(filePath);

        }

    }
}
