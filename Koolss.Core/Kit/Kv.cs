﻿using System;
using System.Collections.Generic;
using System.Text;


/**
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Koolss.Core.Kit
*│　类    名： Kv
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：字典操作类
*│　作    者：Koolss
*│　版    本：1.0.0
*│　邮    箱：koolss@koolss.com
*│　创建时间：2020/3/31 星期二 11:24:29
*│　机器名称：HL
*└──────────────────────────────────────────────────────────────┘
*/

namespace Koolss.Core.Kit
{
    public class Kv : Dictionary<Object,Object>
    {
        public Kv() {

        }

        public static Kv CreateKv() {
            return new Kv();
        }

        public Kv Set(Object key, Object value){
            base.Add(key, value);
            return this;
        }

        public static Kv By(Object key, Object value) {
            return (new Kv()).Set(key, value);
        }

        public Kv Delete(Object key) {
            base.Remove(key);
            return this;
        }
    }
}
