﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;


/**
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Koolss.Core.Crypto
*│　类    名： DigestAlgorithm
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：摘要算法类型
*│　作    者：Koolss
*│　版    本：1.0.0
*│　邮    箱：koolss@koolss.com
*│　创建时间：2020/5/8 17:13:18
*│　机器名称：DESKTOP-PST79O6
*└──────────────────────────────────────────────────────────────┘
*/

namespace Koolss.Core.Crypto
{
    public enum DigestAlgorithmEnum
    {
        [Description("MD2")]
        MD2,
        [Description("MD5")]
        MD5,
        [Description("SHA-1")]
        SHA1,
        [Description("SHA-256")]
        SHA256,
        [Description("SHA-384")]
        SHA384,
        [Description("SHA-512")]
        SHA512
    }

    public static class DigestAlgorithmEnumExtensions
    {
        public static string ToDescriptionString(this DigestAlgorithmEnum val)
        {
            DescriptionAttribute[] attributes = (DescriptionAttribute[])val
               .GetType()
               .GetField(val.ToString())
               .GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : string.Empty;
        }
    }
}
