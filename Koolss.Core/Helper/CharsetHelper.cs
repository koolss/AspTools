﻿using System;
using System.Collections.Generic;
using System.Text;


/**
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Koolss.Core.Util
*│　类    名： CharsetUtil
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：
*│　作    者：Koolss
*│　版    本：1.0.0
*│　邮    箱：koolss@koolss.com
*│　创建时间：2019/11/21 星期四 16:58:23
*│　机器名称：HL
*└──────────────────────────────────────────────────────────────┘
*/

namespace Koolss.Core.Helper
{
    public class CharsetHelper
    {
        /** UTF-8 */
        public static readonly string UTF_8 = "UTF-8";
        /** UTF-8 */
        //public static readonly Char CHARSET_UTF_8 = "utf-8";
    }
}
