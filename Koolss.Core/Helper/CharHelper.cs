﻿using System;
using System.Collections.Generic;
using System.Text;


/**
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Koolss.Core.Util
*│　类    名： ChartUtil
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：
*│　作    者：Koolss
*│　版    本：1.0.0
*│　邮    箱：koolss@koolss.com
*│　创建时间：2019/11/21 星期四 16:23:27
*│　机器名称：HL
*└──────────────────────────────────────────────────────────────┘
*/

namespace Koolss.Core.Helper
{
    public class CharHelper
    {
        /// <summary>
        /// 是否空白符
        /// 空白符包括空格、制表符、全角空格和不间断空格<br>
        /// </summary>
        /// <param name="c">字符</param>
        /// <returns>是否是空白</returns>
        public static bool IsBlankChar(char c)
        {
            return Char.IsWhiteSpace(c) || c == '\ufeff' || c == '\u202a';
        }

    }
}
