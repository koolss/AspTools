﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;


/**
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Koolss.Core.Helper
*│　类    名： JsonHelper
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：
*│　作    者：Koolss
*│　版    本：1.0.0
*│　邮    箱：koolss@koolss.com
*│　创建时间：2020/5/7 13:36:48
*│　机器名称：DESKTOP-PST79O6
*└──────────────────────────────────────────────────────────────┘
*/

namespace Koolss.Core.Helper
{
    public class JsonHelper
    {
        /// <summary>
        /// 创建JObject
        /// </summary>
        /// <returns></returns>
        public JObject CreateObj() 
        {
            return new JObject();
        }

        /// <summary>
        /// 创建JArray
        /// </summary>
        /// <returns></returns>
        public JArray CreateArray() 
        {
            return new JArray();
        }

        /// <summary>
        /// JSON字符串转JObject对象
        /// </summary>
        /// <param name="jsonStr"></param>
        /// <returns></returns>
        public JObject ParseObj(string jsonStr) 
        {
            return new JObject(jsonStr);
        }

        /// <summary>
        /// JSON字符串转JArray对象
        /// </summary>
        /// <param name="jsonStr"></param>
        /// <returns></returns>
        public JArray ParseArray(string jsonStr) 
        {
            return new JArray(jsonStr);
        }

        /// <summary>
        /// 对象转JSON字符串
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToJsonStr(Object obj)
        {
            if (obj == null)
            {
                return "";
            }
            return JsonConvert.SerializeObject(obj);
        }

        /// <summary>
        /// SON字符串转为实体类对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="jsonStr"></param>
        /// <returns></returns>
        public static T ToBean<T>(string jsonStr) where T : class
        {
            return JsonConvert.DeserializeObject<T>(jsonStr);
        }
    }
}
