﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Text;

/**
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Koolss.Core.Util
*│　类    名： CallerUtil
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：调用者。可以通过此类的方法获取调用者、多级调用者以及判断是否被调用
*│　作    者：Koolss
*│　版    本：1.0.0
*│　邮    箱：koolss@koolss.com
*│　创建时间：2019/11/15 0:01:11
*│　机器名称：DESKTOP-PST79O6
*└──────────────────────────────────────────────────────────────┘
*/
namespace Koolss.Core.Helper
{
    public class CallerHelper
    {
        /// <summary>
        /// 获取调用者
        /// </summary>
        /// <returns>调用者</returns>
        public static StackFrame GetCaller()
        {
            StackTrace trace = new StackTrace();
            return trace.GetFrame(1);
        }

        /// <summary>
        /// 获取调用者，指定上级级别
        /// </summary>
        /// <param name="super">上级级别:1代表上级，2代表上上级，以此类推</param>
        /// <returns></returns>
        public static StackFrame GetCaller(int super)
        {
            StackTrace trace = new StackTrace();
            return trace.GetFrame(super);
        }

        /// <summary>
        /// 获得调用者的调用者的声明类型
        /// </summary>
        /// <param name="super">上级级别:1代表上级，2代表上上级，以此类推</param>
        /// <returns></returns>
        public static Type GetCallerType(int super)
        {
            StackTrace trace = new StackTrace();
            return trace.GetFrame(super).GetMethod().DeclaringType;
        }
    }
}
