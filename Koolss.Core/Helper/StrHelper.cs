﻿using System;
using System.Collections.Generic;
using System.Text;


/**
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Koolss_Core.Util
*│　类    名： StrUtil
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：字符串工具类
*│　作    者：Koolss
*│　版    本：1.0.0
*│　邮    箱：koolss@koolss.com
*│　创建时间：2019/11/15 星期五 14:29:02
*│　机器名称：HL
*└──────────────────────────────────────────────────────────────┘
*/

namespace Koolss.Core.Helper
{
    /// <summary>
    /// 字符串工具类
    /// </summary>
    public class StrHelper
    {
        public static readonly string EMPTY = "";
        public static readonly string SLASH = "/";

        /// <summary>
        /// 字符串是否为空白 空白的定义如下：
        /// 1、为null
        /// 2、为不可见字符（如空格）
        /// 3、""
        /// </summary>
        /// <param name="str">被检测的字符串</param>
        /// <returns>是否为空</returns>
        public static bool IsBlank(string str)
        {
            int length;
            if ((str == null) || ((length = str.Length) == 0))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 字符串是否为空，空的定义如下:
        /// 1、为null
        /// 2、为""
        /// </summary>
        /// <param name="str">被检测的字符串</param>
        /// <returns>是否为空</returns>
        public static bool IsEmpty(string str)
        {
            return str == null || str.Length == 0;
        }

        /// <summary>
        /// 创建StringBuilder对象
        /// </summary>
        /// <returns>StringBuilder对象</returns>
        public static StringBuilder Builder()
        {
            return new StringBuilder();
        }

        /// <summary>
        /// 创建StringBuilder对象
        /// </summary>
        /// <param name="capacity">初始大小</param>
        /// <returns>StringBuilder对象</returns>
        public static StringBuilder Builder(int capacity)
        {
            return new StringBuilder(capacity);
        }

        /// <summary>
        /// 去掉指定后缀
        /// </summary>
        /// <param name="str">字符串</param>
        /// <param name="suffix">后缀</param>
        /// <returns>切掉后的字符串，若后缀不是 suffix， 返回原字符串</returns>
        public static string RemoveSuffix(string str, string suffix)
        {
            if (IsEmpty(str) || IsEmpty(suffix))
            {
                return Str(str);
            }
            string str2 = str.ToString();

            if (str2.EndsWith(suffix.ToString()))
            {
                return SubPre(str2, str2.Length - suffix.Length);
            }
            return str2;
        }

        /// <summary>
        /// 切割指定位置之前部分的字符串
        /// </summary>
        /// <param name="str">字符串</param>
        /// <param name="toIndex">切割到的位置（不包括）</param>
        /// <returns>切割后的剩余的前半部分字符串</returns>
        public static string SubPre(string str, int toIndex)
        {
            return Sub(str, 0, toIndex);
        }

        /// <summary>
        /// 改进 subString
        /// index从0开始计算，最后一个字符为-1
        /// 如果from和to位置一样，返回 "" 
        /// 如果from或to为负数，则按照length从后向前数位置，如果绝对值大于字符串长度，则from归到0，to归到length
        /// 如果经过修正的index中from大于to，则互换from和to example: 
        /// abcdefgh 2 3 =》 c 
        /// abcdefgh 2 -3 =》 cde 
        /// </summary>
        /// <param name="str">String</param>
        /// <param name="fromIndex">开始的index（包括）</param>
        /// <param name="toIndex">结束的index（不包括）</param>
        /// <returns>字串</returns>
        public static string Sub(string str, int fromIndex, int toIndex)
        {
            if (IsEmpty(str))
            {
                return Str(str);
            }
            int len = str.Length;

            if (fromIndex < 0)
            {
                fromIndex = len + fromIndex;
                if (fromIndex < 0)
                {
                    fromIndex = 0;
                }
            }
            else if (fromIndex > len)
            {
                fromIndex = len;
            }

            if (toIndex < 0)
            {
                toIndex = len + toIndex;
                if (toIndex < 0)
                {
                    toIndex = len;
                }
            }
            else if (toIndex > len)
            {
                toIndex = len;
            }

            if (toIndex < fromIndex)
            {
                int tmp = fromIndex;
                fromIndex = toIndex;
                toIndex = tmp;
            }

            if (fromIndex == toIndex)
            {
                return EMPTY;
            }

            return str.ToString().Substring(fromIndex, toIndex);
        }

        /// <summary>
        /// 转为字符串，null安全
        /// </summary>
        /// <param name="str">字符串</param>
        /// <returns>字符串</returns>
        public static string Str(string str)
        {
            return str == null ? null : str.ToString();
        }

        /// <summary>
        /// 除去字符串头尾部的空白，如果字符串是<code>null</code>，依然返回<code>null</code>。
        /// 注意，和<code>String.trim</code>不同，此方法使用<code>NumberUtil.isBlankChar</code> 来判定空白， 因而可以除去英文字符集之外的其它空白，如中文空格。
        /// <pre>
        /// trim(null)          = null
        /// trim(&quot;&quot;)            = &quot;&quot;
        /// trim(&quot;     &quot;)       = &quot;&quot;
        /// trim(&quot;abc&quot;)         = &quot;abc&quot;
        /// trim(&quot;    abc    &quot;) = &quot;abc&quot;
        /// </pre>
        /// </summary>
        /// <param name="str">要处理的字符串</param>
        /// <returns>除去头尾空白的字符串，如果原字串为<code>null</code>，则返回<code>null</code></returns>
        public static string Trim(string str)
        {
            return (null == str) ? null : Trim(str, 0);
        }

        /// <summary>
        /// 除去字符串头尾部的空白符，如果字符串是<code>null</code>，依然返回<code>null</code>。
        /// </summary>
        /// <param name="str">要处理的字符串</param>
        /// <param name="mode"><code>-1</code>表示trimStart，<code>0</code>表示trim全部， <code>1</code>表示trimEnd</param>
        /// <returns>除去指定字符后的的字符串，如果原字串为<code>null</code>，则返回<code>null</code></returns>
        public static string Trim(string str, int mode)
        {
            //if (str == null)
            //{
            //    return null;
            //}

            //int length = str.Length;
            //int start = 0;
            //int end = length;

            //// 扫描字符串头部
            //if (mode <= 0)
            //{
            //    while ((start < end) && (CharUtil.IsBlankChar(str.ToCharArray())))
            //    {
            //        start++;
            //    }
            //}

            //// 扫描字符串尾部
            //if (mode >= 0)
            //{
            //    while ((start < end) && (CharUtil.IsBlankChar(str.charAt(end - 1))))
            //    {
            //        end--;
            //    }
            //}

            //if ((start > 0) || (end < length))
            //{
            //    return str.ToString().Substring(start, end);
            //}

            return str.Trim();
        }
    }
}
