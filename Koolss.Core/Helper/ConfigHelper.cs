﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;

/**
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Koolss.Core.Util
*│　类    名： ConfigUtil
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：
*│　作    者：Koolss
*│　版    本：1.0.0
*│　邮    箱：koolss@koolss.com
*│　创建时间：2019/11/19 16:50:26
*│　机器名称：DESKTOP-PST79O6
*└──────────────────────────────────────────────────────────────┘
*/
namespace Koolss.Core.Helper
{
    public class ConfigHelper
    {
        /// <summary>
        /// 获取appsettings.json文件对应的value
        /// </summary>
        /// <param name="key">键值 示例 "Email:Host"</param>
        /// <returns></returns>
        public static string GetAppsettingsJsonValue(string key)
        {
            //添加 json 文件路径
            var builder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            //创建配置根对象
            var configurationRoot = builder.Build();

            IConfigurationSection section = configurationRoot.GetSection(key);

            return section.Value;
        }
    }
}
