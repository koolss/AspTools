﻿using System;
using System.Collections.Generic;
using System.Text;


/**
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Koolss.Core.Model.ResultModel
*│　类    名： Class1
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：
*│　作    者：Koolss
*│　版    本：1.0.0
*│　邮    箱：koolss@koolss.com
*│　创建时间：2020/3/31 星期二 9:22:36
*│　机器名称：HL
*└──────────────────────────────────────────────────────────────┘
*/

namespace Koolss.Core.Model.ResultModel
{
    /// <summary>
    /// 返回状态枚举
    /// </summary>
    public enum ResultStatus
    {
        /// <summary>
        /// 失败
        /// </summary>
        Fail = 500,
        /// <summary>
        /// 未授权
        /// </summary>
        NotAuth = 401,
        /// <summary>
        /// 不接受的请求（参数校验失败）
        /// </summary>
        NotAccept = 406,
        /// <summary>
        /// 成功
        /// </summary>
        Succeed = 200
    }
}
