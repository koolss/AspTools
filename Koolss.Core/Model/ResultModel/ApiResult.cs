﻿using System;
using System.Collections.Generic;
using System.Text;


/**
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Koolss.Core.Model.ResultModel
*│　类    名： OperateResult
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：
*│　作    者：Koolss
*│　版    本：1.0.0
*│　邮    箱：koolss@koolss.com
*│　创建时间：2020/3/31 星期二 9:23:33
*│　机器名称：HL
*└──────────────────────────────────────────────────────────────┘
*/

namespace Koolss.Core.Model.ResultModel
{
    /// <summary>
    /// 返回结果
    /// </summary>
    public class ApiResult
    {
        /// <summary>
        /// 响应状态
        /// </summary>
        public ResultStatus Status { get; set; }
        /// <summary>
        /// 响应信息
        /// </summary>
        public string Msg { get; set; }
        /// <summary>
        /// 响应数据
        /// </summary>
        public object Data { get; set; }
        /// <summary>
        /// 总条数
        /// </summary>
        public int TotalCount { get; set; }

        /// <summary>
        /// 设置API调用结果为成功
        /// </summary>
        /// <returns></returns>
        public static ApiResult Succeed()
        {
            return Succeed("Success");
        }

        /// <summary>
        /// 设置API调用结果为成功
        /// </summary>
        /// <param name="msg">消息</param>
        /// <returns></returns>
        public static ApiResult Succeed(string msg)
        {

            return Succeed(ResultStatus.Succeed, msg);
        }

        /// <summary>
        /// 设置API调用结果为成功
        /// </summary>
        /// <param name="resultStatus">状态码</param>
        /// <param name="msg">消息</param>
        /// <returns></returns>
        public static ApiResult Succeed(ResultStatus resultStatus, string msg)
        {

            return new ApiResult
            {
                Status = resultStatus,
                Msg = msg,
                Data = string.Empty,
                TotalCount = 0
            };
        }

        /// <summary>
        /// 设置API调用结果为成功
        /// </summary>
        /// <returns></returns>
        public ApiResult SucceedResult()
        {
            return SucceedResult("Success");
        }

        /// <summary>
        /// 设置API调用结果为成功
        /// </summary>
        /// <returns></returns>
        public ApiResult SucceedResult(string msg)
        {

            Status = ResultStatus.Succeed;
            Msg = msg;
            Data = string.Empty;
            TotalCount = 0;
            return this;
        }

        /// <summary>
        /// 设置API调用结果为失败
        /// </summary>
        /// <returns></returns>
        public static ApiResult Fail()
        {
            return Fail("Fail");
        }

        /// <summary>
        /// 设置API调用结果为失败
        /// </summary>
        /// <param name="errorMessage">错误消息</param>
        /// <returns></returns>
        public static ApiResult Fail(string errorMessage)
        {
            return Fail(ResultStatus.Fail, errorMessage);
        }

        /// <summary>
        /// 设置API调用结果为失败
        /// </summary>
        /// <param name="resultStatus">错误代码</param>
        /// <param name="errorMessage">错误消息</param>
        /// <returns></returns>
        public static ApiResult Fail(ResultStatus resultStatus, string errorMessage)
        {
            return new ApiResult
            {
                Status = resultStatus,
                Msg = errorMessage,
                Data = string.Empty,
                TotalCount = 0
            };
        }

    }

    /// <summary>
    /// API返回值数据传输对象（泛型版）
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ApiResult<T> : ApiResult
    {
        public virtual T Data { get; set; }

        /// <summary>
        /// 设置API调用结果为成功
        /// </summary>
        /// <param name="t">泛型对象</param>
        /// <returns></returns>
        public static ApiResult<T> Succeed(T t)
        {
            var result = new ApiResult<T>();
            result.SucceedResult().Data = t.GetType().Name;
            result.Data = t;
            return result;
        }

        /// <summary>
        /// 设置API调用结果为成功
        /// </summary>
        /// <param name="t">泛型对象</param>
        /// <param name="totalCount">总行数</param>
        /// <returns></returns>
        public static ApiResult<T> Succeed(T t, int totalCount)
        {
            var result = new ApiResult<T>();
            result.SucceedResult().Data = t.GetType().Name;
            result.Data = t;
            result.TotalCount = totalCount;
            return result;
        }

        /// <summary>
        /// 失败
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static ApiResult<T> Fail(string msg)
        {
            var result = new ApiResult<T>();
            result.Msg = msg;
            return result;
        }
    }
}
