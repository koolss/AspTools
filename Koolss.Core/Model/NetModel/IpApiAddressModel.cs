﻿using System;
using System.Collections.Generic;
using System.Text;


/**
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Koolss.Core.Model.Net
*│　类    名： IpApiAddressModel
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：
*│　作    者：Koolss
*│　版    本：1.0.0
*│　邮    箱：koolss@koolss.com
*│　创建时间：2020/6/4 15:36:47
*│　机器名称：DESKTOP-PST79O6
*└──────────────────────────────────────────────────────────────┘
*/

namespace Koolss.Core.Model.NetModel
{
    /// <summary>
    /// 根据IP获取详细地址
    /// </summary>
    public class IpApiAddressModel
    {
        /// <summary>
        /// 
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// 中国
        /// </summary>
        public string Country { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CountryCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Region { get; set; }
        /// <summary>
        /// 四川省
        /// </summary>
        public string RegionName { get; set; }
        /// <summary>
        /// 成都
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Zip { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public double Lat { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public double Lon { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Timezone { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Isp { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Org { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string @As { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Query { get; set; }
    }
}
