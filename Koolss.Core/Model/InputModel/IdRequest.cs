﻿using System;
using System.Collections.Generic;
using System.Text;


/**
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Koolss.Core.Model.InputModel
*│　类    名： IdRequest
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：
*│　作    者：Koolss
*│　版    本：1.0.0
*│　邮    箱：koolss@koolss.com
*│　创建时间：2020/3/31 星期二 9:32:25
*│　机器名称：HL
*└──────────────────────────────────────────────────────────────┘
*/

namespace Koolss.Core.Model.InputModel
{
    /// <summary>
    /// ID操作请求
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class IdRequest<T> 
    {
        /// <summary>
        /// ID
        /// </summary>
        public T Id { get; set; }
    }

    /// <summary>
    /// 通用request
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class IdsRequest<T>
    {
        public List<T> Ids { get; set; }
    }
}
