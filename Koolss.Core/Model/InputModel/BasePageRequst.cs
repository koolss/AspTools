﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;


/**
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Koolss.Core.Model.InputModel
*│　类    名： BasePageRequst
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：通用分页插件
*│　作    者：Koolss
*│　版    本：1.0.0
*│　邮    箱：koolss@koolss.com
*│　创建时间：2020/3/31 星期二 11:08:50
*│　机器名称：HL
*└──────────────────────────────────────────────────────────────┘
*/

namespace Koolss.Core.Model.InputModel
{
    /// <summary>
    /// 通用分页插件
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BasePageRequst<T>
    {
        /// <summary>
        /// 页数 默认1
        /// </summary>
        public int Page { get; set; }

        /// <summary>
        /// 每页条数 默认10
        /// </summary>
        public int Limit { get; set; }

        /// <summary>
        /// 扩展参数
        /// </summary>
        public JObject JsonObject { get; set; }



    }
}
