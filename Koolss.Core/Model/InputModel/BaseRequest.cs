﻿using System;
using System.Collections.Generic;
using System.Text;


/**
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Koolss.Core.Model.InputModel
*│　类    名： BaseRequest
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：
*│　作    者：Koolss
*│　版    本：1.0.0
*│　邮    箱：koolss@koolss.com
*│　创建时间：2020/5/7 13:33:19
*│　机器名称：DESKTOP-PST79O6
*└──────────────────────────────────────────────────────────────┘
*/

namespace Koolss.Core.Model.InputModel
{
    public class BaseRequest
    {
        /// <summary>
        /// 用户ID
        /// </summary>
        public int UserId { get; set; }
    }
}
