﻿using Koolss.Core.Helper;
using Koolss.Core.Util;
using Koolss.Core.Model;
using Koolss.Core.Model.NetModel;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;


/**
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Koolss.Core.Net
*│　类    名： IpUtil
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：
*│　作    者：Koolss
*│　版    本：1.0.0
*│　邮    箱：koolss@koolss.com
*│　创建时间：2019/11/21 星期四 14:41:58
*│　机器名称：HL
*└──────────────────────────────────────────────────────────────┘
*/

namespace Koolss.Core.Net
{
    public class IpHelper
    {
        /// <summary>
        /// 隐藏掉IP地址的最后一部分为 * 代替
        /// </summary>
        /// <param name="ip">IP地址</param>
        /// <returns>隐藏部分后的IP</returns>
        public static string HideIpPart(string ip)
        {
            return StrHelper.Builder(ip.Length).Append(ip,0,ip.LastIndexOf(".")+1).Append("*").ToString();
        }

        /// <summary>
        /// 根据域名得到IP
        /// </summary>
        /// <param name="hostName">HOST</param>
        /// <returns>IP地址</returns>
        public static string GetIpByHost(string hostName)
        {
            IPHostEntry iPHostEntry = Dns.GetHostEntry(hostName);
            IPAddress iPAddress = iPHostEntry.AddressList[0];
            return iPAddress.ToString();
        }

        /// <summary>
        /// 检查IP地址格式
        /// </summary>
        /// <param name="ip"></param>
        /// <returns></returns>
        public static bool IsIP(string ip)
        {
            return System.Text.RegularExpressions.Regex.IsMatch(ip, @"^((2[0-4]\d|25[0-5]|[01]?\d\d?)\.){3}(2[0-4]\d|25[0-5]|[01]?\d\d?)$");
        }

        /// <summary>
        /// 根据IP地址查询详细地址
        /// </summary>
        /// <param name="ip">IP地址</param>
        /// <returns></returns>
        public static IpApiAddressModel GetAddressByIp(string ip)
        {
            var result = HttpHelper.Get("http://ip-api.com/json/" + ip + "?lang=zh-CN ");
            if (result == null)
            {
                return null;
            }
            var model = JsonHelper.ToBean<IpApiAddressModel>(result);
            return model;
        }
    }
}
