﻿using Koolss.Core.Helper;
using Koolss.Log;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;

/**
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Koolss.Core.Util
*│　类    名： NetUtil
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：
*│　作    者：Koolss
*│　版    本：1.0.0
*│　邮    箱：koolss@koolss.com
*│　创建时间：2019/11/19 16:04:40
*│　机器名称：DESKTOP-PST79O6
*└──────────────────────────────────────────────────────────────┘
*/
namespace Koolss.Core.Util
{
    public class NetHelper
    {
        #region 发送电子邮件
        /// <summary>
        /// 发送电子邮件,所有SMTP配置信息均在config配置文件中system.net节设置.
        /// </summary>
        /// <param name="receiveEmail">接收电子邮件的地址</param>
        /// <param name="msgSubject">电子邮件的标题</param>
        /// <param name="msgBody">电子邮件的正文</param>
        /// <param name="IsEnableSSL">是否开启SSL</param>
        public static bool SendEmail(string receiveEmail, string msgSubject, string msgBody, bool IsEnableSSL)
        {
            //创建电子邮件对象
            MailMessage email = new MailMessage();
            //设置接收人的电子邮件地址
            email.To.Add(receiveEmail);
            //设置邮件的标题
            email.Subject = msgSubject;
            //设置邮件的正文
            email.Body = msgBody;
            //设置邮件为HTML格式
            email.IsBodyHtml = true;
            //发件人邮箱地址。
            email.From = new MailAddress(ConfigHelper.GetAppsettingsJsonValue("Email:Name"));

            //创建SMTP客户端，将自动从配置文件中获取SMTP服务器信息
            SmtpClient smtp = new SmtpClient();
            smtp.Host = ConfigHelper.GetAppsettingsJsonValue("Email:Host");
            //开启SSL
            smtp.EnableSsl = IsEnableSSL;
            //验证发件人身份(发件人的邮箱，邮箱里的生成授权码);
            smtp.Credentials = new NetworkCredential(ConfigHelper.GetAppsettingsJsonValue("Email:Name"), ConfigHelper.GetAppsettingsJsonValue("Email:Password"));

            try
            {
                //发送电子邮件
                smtp.Send(email);

                return true;
            }
            catch (Exception ex)
            {
                LogHelper.ErrorFormat("发送邮件异常：{0}",ex.Message);
                throw ex;
            }
        }

        #endregion
    }
}
