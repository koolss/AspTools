﻿
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Text;


/**
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Koolss.Swagger.Filters
*│　类    名： AssignOperationVendorFilter
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：
*│　作    者：Koolss
*│　版    本：1.0.0
*│　邮    箱：koolss@koolss.com
*│　创建时间：2020/3/26 13:09:22
*│　机器名称：DESKTOP-PST79O6
*└──────────────────────────────────────────────────────────────┘
*/

namespace Koolss.Swagger.Filters
{
    public class AssignOperationVendorFilter : IOperationFilter
    {
        private List<IParameter> defParameters = new List<IParameter>()
        {
            new NonBodyParameter()
            {
                Name = "Authorization",
                In = "header",
                Default = "",
                Type = "string"
            }
            ,new NonBodyParameter()
            {
                Name = "X-Requested-With",
                In = "header",
                Default = "XMLHttpRequest",
                Type = "string"
            }
        };
        public AssignOperationVendorFilter() { }
        public AssignOperationVendorFilter(List<IParameter> parameters)
        {
            defParameters.AddRange(parameters);
        }
        /// <summary>
        /// apply
        /// </summary>
        /// <param name="operation"></param>
        /// <param name="context"></param>
        public void Apply(Operation operation, OperationFilterContext context)
        {
            if (operation == null || context == null)
                return;
            if (operation.Parameters == null)
                operation.Parameters = new List<IParameter>();
            foreach (var item in defParameters)
            {
                operation.Parameters.Add(item);
            }
        }
    }
}
