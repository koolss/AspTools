﻿using Koolss.Core.Util;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Koolss_UnitTest.Koolss.Core.Util
*│　类    名： NetUtilUnit
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：
*│　作    者：Koolss
*│　版    本：1.0.0
*│　邮    箱：koolss@koolss.com
*│　创建时间：2019/11/19 16:05:24
*│　机器名称：DESKTOP-PST79O6
*└──────────────────────────────────────────────────────────────┘
*/
namespace Koolss_UnitTest.Koolss.Core.Util
{
    [TestClass]
    public class NetUtilUnit
    {
        [TestMethod]
        public void SendEmail()
        {
            NetHelper.SendEmail("koolss@koolss.com", "正文标题", "内容", false);
        }
    }
}
