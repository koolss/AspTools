﻿using Koolss.Core.Helper;
using Koolss.Core.Util;
using Koolss.Log;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/**
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Koolss_UnitTest.Koolss_Core.Util
*│　类    名： DictUtilUnit
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：
*│　作    者：Koolss
*│　版    本：1.0.0
*│　邮    箱：koolss@koolss.com
*│　创建时间：2019/11/21 星期四 13:42:38
*│　机器名称：HL
*└──────────────────────────────────────────────────────────────┘
*/

namespace Koolss_UnitTest.Koolss.Core.Util
{
    [TestClass]
    public class DictUtilUnit
    {
        [TestMethod]
        public void Join()
        {
            Dictionary<string, object> keyValues = new Dictionary<string, object>();
            keyValues.Add("name", "张三");
            keyValues.Add("age", "12");
            keyValues.Add("sex", "男");
            keyValues.Add("phone", null);

            var str = DictHelper.Join(keyValues, "&", "=", true);

            LogHelper.InfoFormat("Join连接后的字符：{0}", str);
        }

        [TestMethod]
        public void SortJoin()
        {
            Dictionary<string, object> keyValues = new Dictionary<string, object>();
            keyValues.Add("name", "张三");
            keyValues.Add("age", "12");
            keyValues.Add("sex", "男");
            keyValues.Add("phone", null);

            string str = DictHelper.SortJoin(keyValues, "&", "=", true);

            LogHelper.InfoFormat("SortJoin连接后的字符：{0}", str);
        }

    }
}
