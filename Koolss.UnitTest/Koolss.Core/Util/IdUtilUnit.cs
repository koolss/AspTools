﻿using Koolss.Core.Helper;
using Koolss.Core.Util;
using Koolss.Log;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/**
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Koolss_UnitTest.Koolss.Core.Util
*│　类    名： IdUtil
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：
*│　作    者：Koolss
*│　版    本：1.0.0
*│　邮    箱：koolss@koolss.com
*│　创建时间：2019/11/14 21:55:48
*│　机器名称：DESKTOP-PST79O6
*└──────────────────────────────────────────────────────────────┘
*/
namespace Koolss_UnitTest.Koolss.Core.Util
{
    [TestClass]
    public class IdUtilUnit
    {
        [TestMethod]
        public void randomGuid()
        {
            var guid = IdHelper.RandomGuid();
            LogHelper.Info(guid);
        }
    }
}
