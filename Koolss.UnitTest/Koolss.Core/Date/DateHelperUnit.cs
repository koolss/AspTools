﻿using Koolss.Core.Date;
using Koolss.Log;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/**
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Koolss.UnitTest.Koolss.Core.Date
*│　类    名： DateHelperUnit
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：
*│　作    者：Koolss
*│　版    本：1.0.0
*│　邮    箱：koolss@koolss.com
*│　创建时间：2020/5/8 16:30:57
*│　机器名称：DESKTOP-PST79O6
*└──────────────────────────────────────────────────────────────┘
*/

namespace Koolss.UnitTest.Koolss.Core.Date
{
    [TestClass]
    class DateHelperUnit
    {
        [TestMethod]
        public void Date()
        {
            LogHelper.Info(DateHelper.ToDateTime(1588926568000));
            LogHelper.Info(DateHelper.Now()+"---");
            LogHelper.Info(DateHelper.CurrentSeconds());
        }
    }
}
