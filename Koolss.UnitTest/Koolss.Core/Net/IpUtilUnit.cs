﻿using Koolss.Core.Net;
using Koolss.Log;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


/**
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Koolss.UnitTest.Koolss.Core.Net
*│　类    名： IpUtilUnit
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：
*│　作    者：Koolss
*│　版    本：1.0.0
*│　邮    箱：koolss@koolss.com
*│　创建时间：2019/11/21 星期四 15:05:37
*│　机器名称：HL
*└──────────────────────────────────────────────────────────────┘
*/

namespace Koolss.UnitTest.Koolss.Core.Net
{
    [TestClass]
    public class IpUtilUnit : BaseUnit
    {
        [TestMethod]
        public void GetIpByHost()
        {
            var ip = IpHelper.GetIpByHost("home.koolss.com");
            LogHelper.InfoFormat("GetIpByHost:{0}", ip);
        }

        [TestMethod]
        public void HideIpPart()
        {
            var ip = IpHelper.HideIpPart("192.168.0.2");
            LogHelper.InfoFormat("HideIpPart:{0}", ip);
        }
    }
}
