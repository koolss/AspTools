﻿using FluentFTP;
using Koolss.Extra.Ftp;
using Koolss.Log;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;


/**
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Koolss.UnitTest.Koolss.Extra.Ftp
*│　类    名： FtpUnit
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：
*│　作    者：Koolss
*│　版    本：1.0.0
*│　邮    箱：koolss@koolss.com
*│　创建时间：2019/11/22 星期五 9:50:20
*│　机器名称：HL
*└──────────────────────────────────────────────────────────────┘
*/

namespace Koolss.UnitTest.Koolss.Extra.Ftp
{
    [TestClass]
    public class FtpUnit
    {
        FtpInfo ftpInfo = new FtpInfo("127.0.0.1", 21, "admin", "admin");

        [TestMethod]
        public void Ftp()
        {
            
        }

        [TestMethod]
        public void Upload()
        {
            List<string> list = new List<string>();
            list.Add("e:\\dbs\\admin1.txt");
            list.Add("e:\\dbs\\admin2.txt");
            list.Add("e:\\dbs\\admin3.txt");
            var isok = ftpInfo.UploadFiles(list, "/");
            LogHelper.InfoFormat("Upload:{0}", isok);


            //// create an FTP client
            //FtpClient client = new FtpClient("127.0.0.1");


            //// if you don't specify login credentials, we use the "anonymous" user account
            //client.Credentials = new NetworkCredential("admin", "admin");

            //// begin connecting to the server
            //client.Connect();

            //// get a list of files and directories in the "/htdocs" folder
            //foreach (FtpListItem item in client.GetListing("/htdocs"))
            //{

            //    // if this is a file
            //    if (item.Type == FtpFileSystemObjectType.File)
            //    {

            //        // get the file size
            //        long size = client.GetFileSize(item.FullName);

            //    }

            //    // get modified date/time of the file or folder
            //    DateTime time = client.GetModifiedTime(item.FullName);

            //    // calculate a hash for the file on the server side (default algorithm)
            //    FtpHash hash = client.GetHash(item.FullName);

            //}

            //// upload a file
            ////var isok = client.UploadFile(@"E:\dbs\admin.txt", "/htdocs/admin.txt");
            ////LogUtil.InfoFormat("Upload:{0}", isok);

            //// download the file again
            //var isok = client.DownloadFile(@"e:\aaa.txt", "/aaa.txt");
            //client.UploadFile("E:/dbs/admin.txt", "/admin.txt");
            //LogUtil.InfoFormat("DownloadFile:{0}", isok);
        }

        [TestMethod]
        public void Download()
        {
            var isok = ftpInfo.Download("e:\\\\dbs", "/admin.txt");
            LogHelper.InfoFormat("Download:{0}", isok);
        }
    }
}
