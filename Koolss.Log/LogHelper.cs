﻿using log4net;
using log4net.Config;
using log4net.Repository;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;

/**
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Koolss.Log
*│　类    名： LogUtil
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：静态日志类，用于在不引入日志对象的情况下打印日志
*│　作    者：Koolss
*│　版    本：1.0.0
*│　邮    箱：koolss@koolss.com
*│　创建时间：2019/11/14 22:04:49
*│　机器名称：DESKTOP-PST79O6
*└──────────────────────────────────────────────────────────────┘
*/
namespace Koolss.Log
{
    public class LogHelper
    {
        private static ILog logger;
        private static ILoggerRepository LoggerRepository;

        static LogHelper()
        {
            LoggerRepository = LogManager.CreateRepository("Log4netConsolePractice");
            XmlConfigurator.ConfigureAndWatch(LoggerRepository, new FileInfo("log4net.config"));
            //logger = LogManager.GetLogger(LoggerRepository.Name, GetCallerType(2));
        }

        /// <summary>
        /// 输出Debug日志
        /// </summary>
        /// <param name="message">消息</param>
        public static void Debug(object message)
        {
            LogManager.GetLogger(LoggerRepository.Name, GetCallerType(2)).Debug(message);
        }

        /// <summary>
        /// 输出Debug日志
        /// </summary>
        /// <param name="message">消息</param>
        public static void Debug(object message, Exception ex)
        {
            LogManager.GetLogger(LoggerRepository.Name, GetCallerType(2)).Debug(message, ex);
        }

        /// <summary>
        /// 输出Debug日志
        /// </summary>
        /// <param name="message">消息</param>
        /// <param name="args">变量</param>
        public static void DebugFormat(string message, params object[] args)
        {
            LogManager.GetLogger(LoggerRepository.Name, GetCallerType(2)).DebugFormat(message, args);
        }

        public static void Error(object message)
        {
            LogManager.GetLogger(LoggerRepository.Name, GetCallerType(2)).Error(message);
        }

        public static void Error(object message, Exception exception)
        {
            LogManager.GetLogger(LoggerRepository.Name, GetCallerType(2)).Error(message, exception);
        }

        public static void ErrorFormat(string message, params object[] args)
        {
            LogManager.GetLogger(LoggerRepository.Name, GetCallerType(2)).ErrorFormat(message, args);
        }

        public static void Info(object message)
        {
            LogManager.GetLogger(LoggerRepository.Name, GetCallerType(2)).Info(message);
        }

        public static void Info(object message, Exception ex)
        {
            LogManager.GetLogger(LoggerRepository.Name, GetCallerType(2)).Info(message, ex);
        }

        public static void InfoFormat(string message, params object[] args)
        {
            LogManager.GetLogger(LoggerRepository.Name, GetCallerType(2)).InfoFormat(message, args);
        }

        public static void Warn(object message)
        {
            LogManager.GetLogger(LoggerRepository.Name, GetCurrentMethodFullName()).Warn(message);
        }

        public static void Warn(object message, Exception ex)
        {
            LogManager.GetLogger(LoggerRepository.Name, GetCurrentMethodFullName()).Warn(message, ex);
        }

        public static void WarnFormat(string message, params object[] args)
        {
            LogManager.GetLogger(LoggerRepository.Name, GetCurrentMethodFullName()).WarnFormat(message, args);
        }

        /// <summary>
        /// 获得调用者的调用者的声明类型
        /// </summary>
        /// <param name="super">上级级别:1代表上级，2代表上上级，以此类推</param>
        /// <returns></returns>
        private static Type GetCallerType(int super)
        {
            StackTrace trace = new StackTrace();
            return trace.GetFrame(super).GetMethod().DeclaringType;
        }

        private static string GetCurrentMethodFullName()
        {
            StackFrame frame;
            string str;
            string str1;
            bool flag;
            try
            {
                int num = 2;
                StackTrace stackTrace = new StackTrace();
                int length = stackTrace.GetFrames().Length;
                do
                {
                    int num1 = num;
                    num = num1 + 1;
                    frame = stackTrace.GetFrame(num1);
                    str = frame.GetMethod().DeclaringType.ToString();
                    flag = (!str.EndsWith("Exception") ? false : num < length);
                }
                while (flag);
                string name = frame.GetMethod().Name;
                str1 = string.Concat(str, ".", name);
            }
            catch
            {
                str1 = null;
            }
            return str1;
        }
    }
}
