﻿using Koolss.Core.Crypto;
using Koolss.Core.Date;
using Koolss.Log;
using System;
using System.Globalization;

namespace Koolss.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            LogHelper.Info(DateHelper.ToDateTime(1588926568000));
            LogHelper.Info(DateHelper.Now() + "---");
            LogHelper.Info(DateHelper.CurrentSeconds());
            LogHelper.Info(DigestAlgorithmEnum.SHA256.ToDescriptionString());

        }
    }
}
