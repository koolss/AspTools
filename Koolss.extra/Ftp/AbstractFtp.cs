﻿using FluentFTP;
using Koolss.Core.Collection;
using Koolss.Core.Io;
using Koolss.Core.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;


/**
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Koolss.extra.Ftp
*│　类    名： AbstractFtp
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：
*│　作    者：Koolss
*│　版    本：1.0.0
*│　邮    箱：koolss@koolss.com
*│　创建时间：2019/11/21 星期四 15:24:22
*│　机器名称：HL
*└──────────────────────────────────────────────────────────────┘
*/

namespace Koolss.Extra.Ftp
{
    public abstract class AbstractFtp
    {

        /// <summary>
        /// 服务器地址
        /// </summary>
        protected string host;

        /// <summary>
        /// 连接端口
        /// </summary>
        protected int port;

        /// <summary>
        /// 登录用户
        /// </summary>
        protected string user;

        /// <summary>
        /// 密码
        /// </summary>
        protected string password;

        /// <summary>
        /// 重试次数
        /// </summary>
        protected int retryAttempts;

        /// <summary>
        /// 
        /// </summary>
        protected Encoding encoding;

        /// <summary>
        /// 如果连接超时的话，重新进行连接
        /// </summary>
        /// <returns></returns>
        public abstract AbstractFtp ReconnectIfTimeout();

        /// <summary>
        /// 打开指定目录
        /// </summary>
        /// <param name="directory">directory</param>
        /// <returns>否打开目录</returns>
        public abstract bool Cd(string directory);

        /// <summary>
        /// 打开上级目录
        /// </summary>
        /// <returns>是否打开上级目录</returns>
        public bool ToParent()
        {
            return Cd("..");
        }

        /// <summary>
        /// 远程当前目录（工作目录）
        /// </summary>
        /// <returns>远程当前目录</returns>
        public abstract string Pwd();

        /// <summary>
        /// 在当前远程目录（工作目录）下创建新的目录
        /// </summary>
        /// <param name="dir">目录名</param>
        /// <returns>是否创建成功</returns>
        public abstract bool Mkdir(string dir);

        /// <summary>
        /// 文件是否存在
        /// </summary>
        /// <param name="path">目录</param>
        /// <returns>是否存在</returns>
        public abstract bool FileExists(string path);

        /// <summary>
        /// 目录是否存在
        /// </summary>
        /// <param name="path">目录</param>
        /// <returns>是否存在</returns>
        public abstract bool DirExists(string path);

        /// <summary>
        /// 遍历某个目录下所有文件和目录，不会递归遍历
        /// </summary>
        /// <param name="path">需要遍历的目录</param>
        /// <returns>文件和目录列表</returns>
        public abstract List<string> Ls(string path);

        /// <summary>
        /// 删除指定目录下的指定文件
        /// </summary>
        /// <param name="path">目录路径</param>
        /// <returns>是否删除成功</returns>
        public abstract bool DelFile(string path);

        /// <summary>
        /// 删除文件夹及其文件夹下的所有文件
        /// </summary>
        /// <param name="dirPath">文件夹路径</param>
        /// <returns>是否删除成功</returns>
        public abstract bool DelDir(String dirPath);

        /// <summary>
        /// 上传单文件
        /// </summary>
        /// <param name="localPath">本地路径(@"D:\abc.txt")</param>
        /// <param name="remoteDic">远端目录("/test")</param>
        /// <returns>是否上传成功</returns>
        public abstract bool Upload(String localPath, string remoteDic);

        /// <summary>
        /// 上传多文件
        /// </summary>
        /// <param name="localFiles">本地路径列表</param>
        /// <param name="remoteDic">远端目录("/test")</param>
        /// <returns>是否上传成功</returns>
        public abstract int UploadFiles(IEnumerable<string> localFiles, string remoteDic);

        /// <summary>
        /// 下载文件
        /// </summary>
        /// <param name="localDir">本地目录(@"D:\test")</param>
        /// <param name="remotePath">远程路径("/test/abc.txt")</param>
        public abstract bool Download(String localDir, string remotePath);

        /// <summary>
        /// 下载文件
        /// </summary>
        /// <param name="localDir">本地目录(@"D:\test")</param>
        /// <param name="remoteFiles">远程路径集合("/test/abc.txt")</param>
        public abstract int DownloadFiles(String localDir, IEnumerable<string> remoteFiles);

        /// <summary>
        /// 断开连接
        /// </summary>
        public abstract void DisConnect();

        /// <summary>
        /// 连接
        /// </summary>
        /// <returns></returns>
        public abstract bool Connect();

        /// <summary>
        /// 检测连接是否有效
        /// </summary>
        /// <returns></returns>
        public abstract bool IsConnected();
    }
}
