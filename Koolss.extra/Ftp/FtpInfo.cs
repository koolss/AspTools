﻿using FluentFTP;
using Koolss.Core.Helper;
using Koolss.Core.Util;
using Koolss.Log;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;


/**
*┌──────────────────────────────────────────────────────────────┐
*│　命名空间： Koolss.extra.Ftp
*│　类    名： Ftp
*└──────────────────────────────────────────────────────────────┘
*┌──────────────────────────────────────────────────────────────┐
*│　描    述：
*│　作    者：Koolss
*│　版    本：1.0.0
*│　邮    箱：koolss@koolss.com
*│　创建时间：2019/11/21 星期四 16:44:37
*│　机器名称：HL
*└──────────────────────────────────────────────────────────────┘
*/

namespace Koolss.Extra.Ftp
{
    public class FtpInfo : AbstractFtp
    {
        /// <summary>
        /// 默认端口21
        /// </summary>
        public static readonly int DEFAULT_PORT = 21;

        /// <summary>
        /// 默认重试次数
        /// </summary>
        public static readonly int DEFAULT_RETRYATTEMPTS = 3;

        /// <summary>
        /// 获取FTPClient客户端对象
        /// </summary>
        public FtpClient client { get; set; }

        #region 构造函数
        public FtpInfo(string host) :this(host, DEFAULT_PORT)
        {
            
        }

        public FtpInfo(string host, int port) : this(host, port, "anonymous", "")
        {

        }
        public FtpInfo(string host,string user, string password) : this(host, DEFAULT_PORT, user, password)
        {

        }

        public FtpInfo(string host, int port, string user, string password) : this(host, port, user, password, Encoding.UTF8)
        {

        }

        public FtpInfo(string host, int port, string user, string password, Encoding encoding) : this(host, port, user, password, Encoding.UTF8, DEFAULT_RETRYATTEMPTS)
        {

        }
        public FtpInfo(string host, int port, string user, string password, Encoding encoding,int retryAttempts)
        {
            this.host = host;
            this.port = port;
            this.user = user;
            this.password = password;
            this.encoding = encoding;
            this.retryAttempts = retryAttempts;
            this.Init();
        }

        #endregion

        #region 初始化客户端
        public FtpInfo Init()
        {
            return this.Init(this.host, this.port, this.user, this.password);
        }

        public FtpInfo Init(string host, int port, string user, string password)
        {

            //创建ftp客户端
            client = new FtpClient();
            //设置编码
            client.Encoding = encoding;
            //设置主机IP
            client.Host = host;
            //设置端口
            client.Port = port;
            //重试次数
            client.RetryAttempts = retryAttempts;
            //用户认证
            client.Credentials = new NetworkCredential(user, password);
            //连接
            client.Connect();
            return this;
        }
        #endregion

        #region 打开指定目录
        /// <summary>
        /// 打开指定目录
        /// </summary>
        /// <param name="directory">directory</param>
        /// <returns>否打开目录</returns>
        public override bool Cd(string directory)
        {
            if (StrHelper.IsBlank(directory))
            {
                return false;
            }

            bool flag = true;
            try
            {
                if (Connect())
                {
                    this.client.SetWorkingDirectory(directory);
                }

            }
            catch (IOException e)
            {
                throw new FtpException(e.Message);
            }
            finally
            {
                DisConnect();
            }
            return flag;
        }
        #endregion

        #region 删除文件夹及其文件夹下的所有文件
        /// <summary>
        /// 删除文件夹及其文件夹下的所有文件
        /// </summary>
        /// <param name="dirPath">文件夹路径</param>
        /// <returns>是否删除成功</returns>
        public override bool DelDir(string dirPath)
        {
            try
            {
                if (Connect())
                {
                    this.client.DeleteDirectory(dirPath);
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                DisConnect();
            }
            
            return !DirExists(dirPath);
        }
        #endregion

        #region 删除指定目录下的指定文件
        /// <summary>
        /// 删除指定目录下的指定文件
        /// </summary>
        /// <param name="path">目录路径</param>
        /// <returns>是否删除成功</returns>
        public override bool DelFile(string path)
        {
            try
            {
                if (Connect())
                {
                    this.client.DeleteDirectory(path);
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                DisConnect();
            }

            return !FileExists(path);
        }
        #endregion

        #region 下载单个文件
        /// <summary>
        /// 下载单个文件
        /// </summary>
        /// <param name="localDir">本地目录(@"D:\test")</param>
        /// <param name="remotePath">远程路径("/test/abc.txt")</param>
        public override bool Download(string localDir, string remotePath)
        {
            bool boolResult = false;
            string strFileName = string.Empty;

            try
            {
                //本地目录不存在，则自动创建
                if (!Directory.Exists(localDir))
                {
                    Directory.CreateDirectory(localDir);
                }
                //取下载文件的文件名
                strFileName = Path.GetFileName(remotePath);
                //拼接本地路径
                localDir = Path.Combine(localDir, strFileName);

                if (Connect())
                {
                    boolResult = this.client.DownloadFile(localDir, remotePath, FtpLocalExists.Overwrite);
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error("DownloadFile->下载文件 异常:" + ex.ToString() + "|*|remotePath:" + remotePath);
            }
            finally
            {
                DisConnect();
            }

            return boolResult;
        }
        #endregion

        #region 下载多文件
        /// <summary>
        /// 下载多文件
        /// </summary>
        /// <param name="localDic">本地目录(@"D:\test")</param>
        /// <param name="remotePath">远程路径列表</param>
        /// <returns></returns>
        public override int DownloadFiles(string localDic, IEnumerable<string> remoteFiles)
        {
            int count = 0;
            if (remoteFiles == null)
            {
                return 0;
            }

            try
            {
                //本地目录不存在，则自动创建
                if (!Directory.Exists(localDic))
                {
                    Directory.CreateDirectory(localDic);
                }

                if (Connect())
                {
                    count = this.client.DownloadFiles(localDic, remoteFiles, FtpLocalExists.Overwrite);
                }
            }
            catch (Exception ex)
            {
                LogHelper.Error("DownloadFiles->下载文件 异常:" + ex.ToString());
            }
            finally
            {
                DisConnect();
            }

            return count;
        }
        #endregion

        #region 遍历某个目录下所有文件和目录，不会递归遍历
        /// <summary>
        /// 遍历某个目录下所有文件和目录，不会递归遍历
        /// </summary>
        /// <param name="path">需要遍历的目录</param>
        /// <returns>文件和目录列表</returns>
        public override List<string> Ls(string path)
        {
            string[] names = this.client.GetNameListing(path);
            return new List<string>(names);
        }
        #endregion

        #region 在当前远程目录（工作目录）下创建新的目录
        /// <summary>
        /// 在当前远程目录（工作目录）下创建新的目录
        /// </summary>
        /// <param name="dir">目录名</param>
        /// <returns>是否创建成功</returns>
        public override bool Mkdir(string dir)
        {
            bool flag = true;
            try
            {
                if (Connect())
                {
                    if (!DirExists(dir))
                    {
                        this.client.CreateDirectory(dir);
                    }
                    flag = this.DirExists(dir);
                }
            }
            catch (IOException e)
            {
                throw new FtpException(e.Message);
            }
            finally
            {
                DisConnect();
            }
            return flag;
        }
        #endregion

        #region 远程当前目录
        /// <summary>
        /// 远程当前目录
        /// </summary>
        /// <returns>远程当前目录</returns>
        public override string Pwd()
        {
            string flag = null;
            try
            {
                if (Connect())
                {
                    flag = this.client.GetWorkingDirectory();
                }
            }
            catch (IOException e)
            {
                throw new FtpException(e.Message);
            }
            finally
            {
                DisConnect();
            }
            return flag;
        }
        #endregion

        #region 如果连接超时的话，重新进行连接
        /// <summary>
        /// 如果连接超时的话，重新进行连接
        /// </summary>
        /// <returns></returns>
        public override AbstractFtp ReconnectIfTimeout()
        {
            string pwd = null;
            try
            {
                pwd = Pwd();
            }
            catch (IOException e)
            {

                throw;
            }
            if (pwd==null)
            {
                return this.Init();
            }
            return this;
        }
        #endregion

        #region 上传单文件
        /// <summary>
        /// 上传单文件
        /// </summary>
        /// <param name="localPath">本地路径(@"D:\abc.txt")</param>
        /// <param name="remoteDic">远端目录("/test")</param>
        /// <returns>是否上传成功</returns>
        public override bool Upload(string localPath, string remoteDic)
        {
            bool boolResult = false;
            FileInfo fileInfo = null;

            try
            {
                //本地路径校验
                if (!File.Exists(localPath))
                {
                    LogHelper.ErrorFormat("UploadFile->本地文件不存在:{0}",localPath);
                    return boolResult;
                }
                else
                {
                    fileInfo = new FileInfo(localPath);
                }
                //远端路径校验
                if (string.IsNullOrEmpty(remoteDic))
                {
                    remoteDic = "/";
                }
                if (!remoteDic.StartsWith("/"))
                {
                    remoteDic = "/" + remoteDic;
                }
                if (!remoteDic.EndsWith("/"))
                {
                    remoteDic += "/";
                }

                //拼接远端路径
                remoteDic += fileInfo.Name;

                if (Connect())
                {
                    boolResult = this.client.UploadFile(localPath, remoteDic, FtpExists.Overwrite, true);
                }
            }
            catch (Exception ex)
            {
                LogHelper.ErrorFormat("UploadFile->上传文件 异常:{0}|*|localPath:{1}", ex.ToString(),localPath);
            }
            finally
            {
                DisConnect();
            }

            return boolResult;
        }

        #endregion

        #region 上传多文件
        /// <summary>
        /// 上传多文件
        /// </summary>
        /// <param name="localFiles">本地路径列表</param>
        /// <param name="remoteDic">远端目录("/test")</param>
        /// <returns></returns>
        public override int UploadFiles(IEnumerable<string> localFiles, string remoteDic)
        {
            int count = 0;
            List<FileInfo> listFiles = new List<FileInfo>();

            if (localFiles == null)
            {
                return 0;
            }

            try
            {
                foreach (string file in localFiles)
                {
                    if (!File.Exists(file))
                    {
                        LogHelper.ErrorFormat("UploadFiles->本地文件不存在:{0}",file);
                        continue;
                    }
                    listFiles.Add(new FileInfo(file));
                }

                //远端路径校验
                if (string.IsNullOrEmpty(remoteDic))
                {
                    remoteDic = "/";
                }
                if (!remoteDic.StartsWith("/"))
                {
                    remoteDic = "/" + remoteDic;
                }
                if (!remoteDic.EndsWith("/"))
                {
                    remoteDic += "/";
                }

                if (Connect())
                {
                    if (listFiles.Count > 0)
                    {
                        count = this.client.UploadFiles(listFiles, remoteDic, FtpExists.Overwrite, true);
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.ErrorFormat("UploadFiles->上传文件 异常:{0}",ex.ToString());
            }
            finally
            {
                DisConnect();
            }

            return count;
        }
        #endregion

        #region 文件是否存在
        /// <summary>
        /// 文件是否存在
        /// </summary>
        /// <param name="path">目录</param>
        /// <returns>是否存在</returns>
        public override bool FileExists(string path)
        {
            bool flag = false;
            try
            {
                if (Connect())
                {
                    flag = this.client.FileExists(path);
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                DisConnect();
            }
            return flag;
        }
        #endregion

        #region FTP是否已连接
        /// <summary>
        /// FTP是否已连接
        /// </summary>
        /// <returns></returns>
        public override bool IsConnected()
        {
            bool result = false;
            if (this.client != null)
            {
                result = this.client.IsConnected;
            }
            return result;
        }
        #endregion

        #region 目录是否存在
        /// <summary>
        /// 目录是否存在
        /// </summary>
        /// <param name="path">目录</param>
        /// <returns>是否存在</returns>
        public override bool DirExists(string path)
        {
            bool flag = false;
            try
            {
                if (Connect())
                {
                    DisConnect();
                    flag = this.client.DirectoryExists(path);
                }
                
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                DisConnect();
            }
            return flag;
            
        }
        #endregion

        #region 断开连接
        /// <summary>
        /// 断开连接
        /// </summary>
        public override void DisConnect()
        {
            if (this.client != null)
            {
                if (this.client.IsConnected)
                {
                    this.client.Disconnect();
                }
            }
        }
        #endregion

        #region 创建连接
        /// <summary>
        /// 创建连接
        /// </summary>
        /// <returns></returns>
        public override bool Connect()
        {
            bool result = false;
            if (this.client != null)
            {
                if (this.client.IsConnected)
                {
                    return true;
                }
                else
                {
                    this.client.Connect();
                    return true;
                }
            }
            return result;
        }
        #endregion
    }
}
